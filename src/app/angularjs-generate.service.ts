import { Http, Headers } from "@angular/http"
import { Injectable } from '@angular/core'
import { Grid } from './shared/grid.model'
import { saveAs } from "file-saver";

@Injectable()
export class AngularjsGenerateService {

  constructor(private http: Http) { }

  public generateAll(gridModel: Array<Grid>): void {
    const baseURL: string = 'http://localhost:4000/generate';
    let model: string = JSON.stringify(gridModel)
    let headers: Headers = new Headers({ 'Content-Type': 'application/json' })
    this.http.post(baseURL, model, { headers: headers, responseType: 3 })
      .toPromise()
      .then((response: any) => {
        saveAs.saveAs(response._body, "dist.zip");
      })
      .then(() => { window.location.reload() })
  }

}
