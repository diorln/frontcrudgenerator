import { Component, OnInit, EventEmitter, Output } from '@angular/core';

@Component({
  selector: 'interisk-botoes',
  templateUrl: './botoes.component.html'
})
export class BotoesComponent implements OnInit {

  @Output() public cancel: EventEmitter<any> = new EventEmitter<any>()
  @Output() public save: EventEmitter<any> = new EventEmitter<any>()

  constructor() { }

  public cancelar = (): void => {
    this.cancel.emit()
  }

  public salvar = (): void => {
    this.save.emit()
  }

  ngOnInit() {
  }

}
