import { Component } from '@angular/core'

@Component({
  selector: 'interisk-topo',
  templateUrl: './topo.component.html'
})
export class TopoComponent {
  public titulo: string = 'Crud generator - Interisk'
}