import { Column } from "./column.model";
export class Grid {


  constructor(
    public is_campo_form_selected: boolean,
    public is_campo_list_selected: boolean,
    public campo: Array<Column>,
    public label: string,
    public controller: any,
    public route: any,
    public service: any,
    public grid: any,
    public form: any,
    public tipo: string,
    public database: string,
    public table: string,
    public selectedModalValue: string,
    public selectedModalDescricao: string
  ) { }
}