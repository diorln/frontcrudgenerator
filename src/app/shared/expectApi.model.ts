export class ExpectApi {

  public controller: any
  public route: any
  public service: any
  public grid: any
  public form: any

  constructor() {
    this.controller = { name: '', primaryKey: '' }
    this.route = { baseStateName: '', categoria: '', pageTitle: '' }
    this.service = { ENTITY_URL: '' }
    this.grid = {
      columnDefs: [],
      model: { headerName: '', field: '', type: 'dimension', width: 150 }
    }
    this.form = null
  }
}