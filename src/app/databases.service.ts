import { Http } from "@angular/http"
import { Injectable } from "@angular/core"
import { Database } from "./shared/database.model"

@Injectable()
export class DatabasesServices {

  constructor(private http: Http) { }

  public getDatabases(): Promise<Database[]> {
    return this.http.get('http://localhost:4000')
      .toPromise()
      .then((resposta: any) => resposta.json())
  }
}
