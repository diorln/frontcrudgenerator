import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpModule } from "@angular/http";
import { AgGridModule } from "ag-grid-angular";
import { FormsModule } from '@angular/forms';

import { AppComponent } from './app.component';
import { TopoComponent } from './topo/topo.component';
import { PainelComponent } from './painel/painel.component';
import { BotoesComponent } from './botoes/botoes.component';
import { FormComponent } from './form/form.component';
import { CamposGridComponent } from './campos-grid/campos-grid.component';
import { InputText } from './campos-grid/inputText.component';
import { InputCheckbox } from './campos-grid/inputCheckbox.component';
import { RendererCheckbox } from "./campos-grid/rendererCheckbox.component";
import { RendererSelect } from "./campos-grid/rendererSelect.component";
import { SelectType } from "./campos-grid/selectType.component";
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';

@NgModule({
  declarations: [
    AppComponent,
    TopoComponent,
    PainelComponent,
    BotoesComponent,
    FormComponent,
    CamposGridComponent,
    InputText,
    InputCheckbox,
    RendererCheckbox,
    RendererSelect,
    SelectType
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpModule,
    NgbModule,
    AgGridModule.withComponents([
      FormComponent,
      InputText,
      InputCheckbox,
      RendererCheckbox,
      RendererSelect,
      SelectType
    ])
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
