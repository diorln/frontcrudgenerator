import { Component, OnInit, ViewChild, ElementRef } from '@angular/core'
// Models
import { Database } from "../shared/database.model"
import { Table } from "../shared/table.model"
import { Column } from "../shared/column.model"
import { Grid } from "../shared/grid.model"
// Services
import { DatabasesServices } from "../databases.service"
import { TablesServices } from "../tables.service"
import { ColumnsServices } from "../columns.service"
import { AngularjsGenerateService } from "../angularjs-generate.service"
import { NgbModalConfig, NgbModal } from '@ng-bootstrap/ng-bootstrap'

@Component({
  selector: 'interisk-form',
  templateUrl: './form.component.html',
  providers: [
    DatabasesServices,
    TablesServices,
    ColumnsServices,
    AngularjsGenerateService,
    NgbModalConfig,
    NgbModal
  ]
})
export class FormComponent implements OnInit {

  public databases: Database[] = []
  public tables: Table[] = []
  public columns: Column[] = []
  public grid: Array<Grid>
  public listPrimaryKey: Array<Grid>

  public selectedBase: string
  public selectedTable: string
  public selectedCategoria: string
  public nomeController: string = ''
  public primaryKey: string = ''
  public pageTitle: string = ''
  public entity_url: string = ''
  public baseStateName: string = ''
  public selectedModalValue: string = ''
  public selectedModalDescricao: string = ''
  public selectedFields: Array<Grid>

  @ViewChild('myModal') public myModal: ElementRef

  constructor(
    public databasesService: DatabasesServices,
    public tablesServices: TablesServices,
    public columnsServices: ColumnsServices,
    public angularjsGenerateService: AngularjsGenerateService,
    public config: NgbModalConfig,
    private modalService: NgbModal
  ) { }

  ngOnInit() {
    this.getDatabases()
  }

  private getDatabases = (): void => {
    this.databasesService
      .getDatabases()
      .then((results: Database[]): void => { this.databases = results })
      .catch((params: any): void => console.log(params))
  }

  private getTables = (selectedDatabase: string): void => {
    this.grid = undefined
    this.tablesServices
      .getTables(selectedDatabase)
      .then((results: Table[]): void => { this.tables = results })
      .catch((params: any): void => console.log(params))
  }

  private getColumns = (selectedDatabase: string, selectedTable: string): void => {
    this.grid = undefined
    this.columnsServices
      .getColumns(selectedDatabase, selectedTable)
      .then((results: Column[]): void => {
        this.columns = results
        this.grid = results.map((column: Column): Grid => {
          return {
            is_campo_list_selected: false, is_campo_form_selected: false, campo: column['COLUMN_NAME'],
            label: '', controller: '', route: '', service: '', grid: null, form: null, tipo: '0', database: this.selectedBase,
            table: this.selectedTable, selectedModalValue: '', selectedModalDescricao: ''
          }
        })
      })
      .catch((params: any): void => console.log(params))
  }

  public atualizaDatabase(selectedBase: string): void {
    if (this.isNotUndefinedEmptyAndNull(selectedBase)) {
      this.selectedBase = selectedBase
      this.getTables(selectedBase)
    }
  }

  public atualizaTable(selectedTable: string): void {
    if (this.isNotUndefinedEmptyAndNull(selectedTable)) {
      this.selectedTable = selectedTable
      this.getColumns(this.selectedBase, selectedTable)
    }
  }

  private isNotUndefinedEmptyAndNull = (word: string): boolean => {
    return word !== null && word !== '' && word !== undefined
  }

  public isVisible = (vet: Array<any>): boolean => {
    return vet.length > 0
  }

  public atualizaListaGrid = (listaAtualizada: Array<Grid>): void => {
    this.selectedModalValue = ''
    this.selectedModalDescricao = ''
    this.grid = listaAtualizada

    let is_some_campo_list_selected: boolean = this.grid
      .some((configForm: Grid) => configForm.is_campo_list_selected === true)

    if (is_some_campo_list_selected) {
      this.listPrimaryKey = this.grid
        .filter((configForm: Grid) => {
          return configForm.is_campo_list_selected === true
        })
    }
  }

  public open = (content): void => {
    let canOpenModal: boolean = this.grid
      .some((configForm: Grid) => configForm.tipo === "3")

    if (canOpenModal === true) {
      this.modalService.open(content)
    } else {
      this.salvarModal()
    }
  }

  public salvar = (): void => {
    let is_some_campo_list_selected: boolean = this.grid
      .some((configForm: Grid) => configForm.is_campo_list_selected === true)
    let is_some_campo_form_selected: boolean = this.grid
      .some((configForm: Grid) => configForm.is_campo_form_selected === true)

    let canSave: boolean = (is_some_campo_list_selected === true && is_some_campo_form_selected === true) ? true : false
    if (canSave) {
      this.selectedFields = this.grid
        .filter((configForm: Grid): boolean => {
          return configForm.is_campo_list_selected === true || configForm.is_campo_form_selected === true
        })
        .map((configForm: Grid): Grid => {
          configForm.database = this.selectedBase
          configForm.table = this.selectedTable
          configForm.controller = { name: this.nomeController, primaryKey: this.primaryKey }
          configForm.route = { baseStateName: this.baseStateName, categoria: this.getCategoriaPorID(this.selectedCategoria), pageTitle: this.pageTitle }
          configForm.service = { ENTITY_URL: this.entity_url }
          if (configForm.is_campo_list_selected === true) {
            configForm.grid = { model: { headerName: configForm.label, field: configForm.campo, type: 'actions', width: 120 } }
          }
          if (configForm.is_campo_form_selected === true) {
            configForm.form = { model: { headerName: configForm.label, field: configForm.campo } }
            if (configForm.tipo == "3") {
              this.config.backdrop = 'static'
              this.config.keyboard = false
              configForm.selectedModalValue = this.selectedModalValue
              configForm.selectedModalDescricao = this.selectedModalDescricao
            }
          }
          return configForm
        })
    } else {
      console.warn(
        'Necessário selecionar ao menos um campo para listagem e um para o form',
        'Necessário preencher a label do campo'
      )
    }
  }

  private getCategoriaPorID = (params: string): string => {
    switch (params) {
      case '1':
        return 'configuracao'
      case '2':
        return 'ferramenta'
      default:
        return 'funcionalidade'
    }
  }

  public cancelar = (): void => {
    window.location.reload()
  }

  public salvarModal = (): void => {
    if (this.isFormValid()) {
      this.angularjsGenerateService.generateAll(this.selectedFields)
      this.modalService.dismissAll()
    }
  }

  private isFormValid = (): boolean => {

    let isFormOk: boolean = this.isDiferentOfEmptyString(this.nomeController) && this.isDiferentOfEmptyString(this.primaryKey) &&
      this.isDiferentOfEmptyString(this.pageTitle) && this.isDiferentOfEmptyString(this.entity_url) &&
      this.isDiferentOfEmptyString(this.baseStateName)

    let isModalFormOk: boolean = this.isDiferentOfEmptyString(this.selectedModalValue) && this.isDiferentOfEmptyString(this.selectedModalDescricao)

    let isAllFormFieldsOk: boolean = isFormOk && isModalFormOk

    return isAllFormFieldsOk || isFormOk

  }

  private isDiferentOfEmptyString = (params: string): boolean => {
    return params !== ''
  }


}
