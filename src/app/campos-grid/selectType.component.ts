import { AfterViewInit, Component, ViewChild, ViewContainerRef } from "@angular/core";

import { ICellEditorAngularComp } from "ag-grid-angular";

@Component({
  selector: 'select-type-cell',
  template: `<select #select [(ngModel)]="value" style="width: 100%;height: 100%;">
              <option value="0">Input</option>
              <option value="1">Checkbox</option>
              <option value="2">Textarea</option>
              <option value="3">Select</option>
            </select>`
})
export class SelectType implements ICellEditorAngularComp, AfterViewInit {
  private params: any;
  public value: string;
  private cancelBeforeStart: boolean = false;

  @ViewChild('select', { read: ViewContainerRef }) public select;

  agInit(params: any): void {
    this.params = params;
    this.value = this.params.value;
  }

  getValue(): any {
    return this.value;
  }

  isCancelBeforeStart(): boolean {
    return this.cancelBeforeStart;
  }

  // will reject the number if it greater than 1,000,000
  // not very practical, but demonstrates the method.
  isCancelAfterEnd(): boolean {
    return false;
  };

  onKeyDown(event): void { }

  // dont use afterGuiAttached for post gui events - hook into ngAfterViewInit instead for this
  ngAfterViewInit() {
    setTimeout(() => {
      this.select.element.nativeElement.focus();
    })
  }

}