import { Component } from "@angular/core";
import { ICellRendererAngularComp } from "ag-grid-angular";

@Component({
    selector: 'renderer-checkbox-cell',
    template: `<span>{{text}}</span>`
})
export class RendererCheckbox implements ICellRendererAngularComp {
    private params: object = {};
    private status: boolean;
    public text: string;

    agInit(params: any): void {
        this.params = params;
        this.setStatus(this.params);
    }

    refresh(params: any): boolean {
        this.params = params;
        this.setStatus(this.params);
        return true;
    }

    private setStatus(params) {
        this.status = params.value;
        this.text = this.status === true ? 'Sim' : 'Não';
    };
}

