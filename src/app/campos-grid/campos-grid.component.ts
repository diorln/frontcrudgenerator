import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core'
import { GridOptions, CellValueChangedEvent } from 'ag-grid-community'
import { InputText } from './inputText.component'
import { InputCheckbox } from './inputCheckbox.component'
import { RendererCheckbox } from './rendererCheckbox.component'
import { Grid } from '../shared/grid.model'
import { SelectType } from "./selectType.component";

@Component({
  selector: 'interisk-campos-grid',
  templateUrl: './campos-grid.component.html'
})
export class CamposGridComponent {

  @Input() public data: Array<Grid>
  @Output() public listaAtualizada: EventEmitter<any> = new EventEmitter<any>()
  private gridOptions: GridOptions

  constructor() {
    this.gridOptions = <GridOptions>{}
    this.gridOptions.columnDefs = [
      { headerName: 'Formulário', cellClass: 'text-center', field: 'is_campo_form_selected', width: 140, cellEditorFramework: InputCheckbox, cellRendererFramework: RendererCheckbox, editable: true },
      { headerName: 'Listagem', cellClass: 'text-center', field: 'is_campo_list_selected', width: 140, cellEditorFramework: InputCheckbox, cellRendererFramework: RendererCheckbox, editable: true },
      { headerName: 'Campo', field: 'campo', width: 270 },
      {
        headerName: 'Tipo', field: 'tipo', width: 270, editable: true, cellEditorFramework: SelectType, cellRenderer: function (params) {
          switch (params.value) {
            case '0':
              return 'Input'
            case '1':
              return 'Checkbox'
            case '2':
              return 'Textarea'
            default:
              return 'Select'
          }
        }
      },
      { headerName: 'Label', field: 'label', width: 270, cellEditorFramework: InputText, editable: true, }
    ]
    this.gridOptions.rowData = []
  }
  ngOnInit() {
    this.gridOptions.rowData = this.data
    this.gridOptions.onCellValueChanged = (event: CellValueChangedEvent): void => {
      let valorNovo = typeof event.newValue === 'string' ? event.newValue.trim() : event.newValue
      let valorAntigo = event.oldValue
      if (valorAntigo !== valorNovo && event.node.data.label !== '') {
        this.listaAtualizada.emit(this.gridOptions.rowData)
      }
    }
  }

}