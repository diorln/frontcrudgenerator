import { Component } from "@angular/core"
import { ICellRendererComp } from 'ag-grid-community'

@Component({
    selector: 'renderer-select-cell',
    template: `<span>{{text}}</span>`
})
export class RendererSelect implements ICellRendererComp {
    
    private params: object = {}
    public text: string

    init(params: any): void {
        this.params = params
        this.setStatus(this.params)
    }

    getGui(): HTMLElement {
        return new HTMLElement()
    }

    refresh(params: any): boolean {
        this.params = params
        this.setStatus(this.params)
        return true
    }

    private setStatus(params) {
        this.text = this.getType(params.value)
    }

    private getType(type: string): string {
        switch (type) {
            case '0':
                return 'Input'
            case '1':
                return 'Checkbox'
            case '2':
                return 'Textarea'
            default:
                return 'Select'
        }
    }

}

