import { Http } from "@angular/http"
import { Injectable } from "@angular/core"
import { Table } from "./shared/table.model"

@Injectable()
export class TablesServices {

  constructor(private http: Http) { }

  public getTables(DATABASE_NAME): Promise<Table[]> {
    return this.http.get(`http://localhost:4000/tabelas?database=${DATABASE_NAME}`)
      .toPromise()
      .then((resposta: any) => resposta.json())
  }
}
