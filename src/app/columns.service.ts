import { Http } from "@angular/http"
import { Injectable } from "@angular/core"
import { Column } from "./shared/column.model"

@Injectable()
export class ColumnsServices {

  constructor(private http: Http) { }

  public getColumns(DATABASE_NAME, TABLE_NAME): Promise<Column[]> {
    return this.http.get(`http://localhost:4000/colunas?database=${DATABASE_NAME}&table=${TABLE_NAME}`)
      .toPromise()
      .then((resposta: any) => resposta.json())
  }
}
